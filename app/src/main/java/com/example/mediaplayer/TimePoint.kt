package com.example.mediaplayer

data class TimePoint(
    val minutes: Int,
    val seconds: Int,
) {
    companion object {
        fun createFrom(durationMilliseconds: Long): TimePoint {
            val durationSeconds = (durationMilliseconds / MILLISECONDS_IN_SECOND).toInt()
            return TimePoint(
                durationSeconds / SECONDS_IN_MINUTE,
                durationSeconds % SECONDS_IN_MINUTE
            )
        }
    }
}