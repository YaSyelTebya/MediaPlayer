package com.example.mediaplayer

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.mediaplayer.databinding.FragmentChooseMediaBinding

class ChooseMediaFragment : Fragment() {
    private var _binding: FragmentChooseMediaBinding? = null
    private val binding get() = _binding!!

    private val getMedia =
        registerForActivityResult(ActivityResultContracts.GetContent()) { mediaUri ->
            mediaUri ?: return@registerForActivityResult

            val playMediaAction = ChooseMediaFragmentDirections
                .actionChooseMediaFragmentToPlayMediaFragment(mediaUri)
            findNavController()
                .navigate(playMediaAction)
        }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentChooseMediaBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.chooseAudioButton.setOnClickListener {
            chooseAudio()
        }

        binding.chooseVideoButton.setOnClickListener {
            chooseVideo()
        }
    }

    private fun chooseAudio() {
        chooseMedia(AUDIO_GENERAL_MIME)
    }

    private fun chooseVideo() {
        chooseMedia(VIDEO_GENERAL_MIME)
    }

    private fun chooseMedia(mediaMimeType: String) {
        getMedia.launch(mediaMimeType)
    }

    private companion object {
        const val AUDIO_GENERAL_MIME = "audio/*"
        const val VIDEO_GENERAL_MIME = "video/*"
    }
}