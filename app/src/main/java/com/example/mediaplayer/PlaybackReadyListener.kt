package com.example.mediaplayer

import androidx.media3.common.Player

class PlaybackReadyListener(private val onPlayerReady: () -> Unit) : Player.Listener {
    override fun onPlaybackStateChanged(playbackState: Int) {
        if (playbackState == Player.STATE_READY) {
            onPlayerReady()
        }
        super.onPlaybackStateChanged(playbackState)
    }
}