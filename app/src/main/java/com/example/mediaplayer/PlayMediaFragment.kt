package com.example.mediaplayer

import android.content.ComponentName
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.media3.common.MediaItem
import androidx.media3.common.Player
import androidx.media3.session.MediaController
import androidx.media3.session.SessionToken
import androidx.navigation.fragment.navArgs
import com.example.mediaplayer.databinding.FragmentPlayMediaBinding
import com.google.common.util.concurrent.ListenableFuture
import com.google.common.util.concurrent.MoreExecutors
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class PlayMediaFragment : Fragment() {
    private var _binding: FragmentPlayMediaBinding? = null
    private val binding get() = _binding!!

    private lateinit var controllerFuture: ListenableFuture<MediaController>

    private val args: PlayMediaFragmentArgs by navArgs()

    private var playerController: MediaController? = null
        set(value) {
            field = value

            if (value != null) {
                setupPlayer(value)

                if (playerIsReady(value)) {
                    onPlayerReady()
                }
            }
        }

    private var playerPositionWatch: Job? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentPlayMediaBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onStart() {
        super.onStart()

        connectOrCreateSession()

        configurePlaybackButton()
    }

    private fun connectOrCreateSession() {
        val sessionToken = SessionToken(
            requireContext(),
            ComponentName(requireContext(), MediaPlayerService::class.java)
        )

        controllerFuture = MediaController.Builder(requireContext(), sessionToken).buildAsync()
        controllerFuture.addListener(
            {
                playerController = controllerFuture.get()
            },
            MoreExecutors.directExecutor()
        )
    }

    private fun configurePlaybackButton() {
        binding.togglePlaybackButton.setOnClickListener {
            onTogglePlayback()
        }
        updatePlaybackButtonState()  // for cases when media playback was paused via notification
    }

    private fun playerIsReady(playerController: MediaController) =
        playerController.playbackState == Player.STATE_READY

    private fun setupPlayer(playerController: MediaController) {
        val playbackReadyListener = PlaybackReadyListener(::onPlayerReady)

        playerController.addListener(playbackReadyListener)

        if (
            isNewMediaRequested(
                playerController.currentMediaItem?.localConfiguration?.uri,
                args.mediaUri
            )
        ) {
            val newMediaItem = MediaItem.fromUri(args.mediaUri)
            setNewMediaItem(playerController, newMediaItem)
        }

        binding.playerView.player = playerController
    }

    private fun setNewMediaItem(playerController: MediaController, newMediaItem: MediaItem) {
        playerController.setMediaItem(newMediaItem)
        playerController.prepare()
    }

    private fun isNewMediaRequested(currentMedia: Uri?, requestedMedia: Uri) =
        currentMedia != requestedMedia

    private fun onPlayerReady() {
        playerPositionWatch?.cancel()

        binding.togglePlaybackButton.isEnabled = true

        updatePlaybackButtonState()

        playerPositionWatch = lifecycleScope.launch {
            followPlayPosition()
        }
    }

    private fun onTogglePlayback() {
        playerController?.let { playerController ->
            if (playerController.isPlaying) {
                playerController.pause()
            } else {
                playerController.play()
            }
            updatePlaybackButtonState()
        }
    }

    private fun updatePlaybackButtonState() {
        if (playerController?.isPlaying == true) {
            setPlaybackButtonText(R.string.pause_label)
        } else {
            setPlaybackButtonText(R.string.play_label)
        }
    }

    private fun setPlaybackButtonText(@StringRes displayLabel: Int) {
        binding.togglePlaybackButton.text = getString(displayLabel)
    }

    private suspend fun followPlayPosition() {
        val duration = playerController!!.duration
        val mediaLength = TimePoint.createFrom(duration)

        binding.playbackProgress.max = duration.toInt()

        var currentPositionData: TimePoint

        var currentPosition: Long

        while (true) {
            currentPosition = playerController!!.currentPosition
            currentPositionData = TimePoint.createFrom(currentPosition)

            updateProgressOnUi(currentPosition, currentPositionData, mediaLength)

            delay(TIMELINE_UPDATE_INTERVAL)
        }
    }

    private fun updateProgressOnUi(
        currentPosition: Long,
        currentPositionData: TimePoint,
        mediaLength: TimePoint
    ) {
        updateSeekBar(currentPosition)

        updateTextView(currentPositionData, mediaLength)
    }

    private fun updateTextView(
        currentPositionData: TimePoint,
        mediaLength: TimePoint
    ) {
        binding.durationAndCurrentTime.text =
            getString(
                R.string.timeline_format,
                currentPositionData.minutes,
                currentPositionData.seconds,
                mediaLength.minutes,
                mediaLength.seconds
            )
    }

    private fun updateSeekBar(currentPosition: Long) {
        binding.playbackProgress.progress = currentPosition.toInt()

        binding.playbackProgress.setOnSeekBarChangeListener(SeekbarChangesListener {
            playerController?.seekTo(it.toLong())
        })
    }

    override fun onStop() {
        MediaController.releaseFuture(controllerFuture)
        super.onStop()
    }

    companion object {
        const val TIMELINE_UPDATE_INTERVAL = 100L
    }
}